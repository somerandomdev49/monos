#include "monos.h"

void setup_memory() {
#if defined(MONOS_PLATFORM_X86_64) // x86-64
	logdev_beg(DL0, "setting up the GDT");
	extern void x86_setup_gdt();
	x86_setup_gdt();
	logdev_end(DL0, NULL);
#else
#error no platform chosen!
#endif
}

void setup_physmm() {
#if defined(MONOS_PLATFORM_X86_64)
	extern void x86_setup_pmm();
	x86_setup_pmm();
#else
#error no platform chosen!
#endif
}

void setup_virtmm() {
#if defined(MONOS_PLATFORM_X86_64)
	extern void x86_setup_vmm();
	x86_setup_vmm();
#else
#error no platform chosen!
#endif
}
