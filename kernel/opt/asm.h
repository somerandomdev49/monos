#ifndef MONOS_X86_64_ASM_H_
#define MONOS_X86_64_ASM_H_
#include "../monos.h"

void opt_printdisasm(void *addr, void *buf, size_t len, size_t count);

#endif
