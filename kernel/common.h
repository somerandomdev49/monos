#ifndef MONOS_COMMON_H_
#define MONOS_COMMON_H_

#include <stdint.h>
#include <stddef.h>

#define MONOS_VERSION "0.2.7a"
#define MONOS_CONTRIBS "monomere"

typedef ptrdiff_t ssize_t;
typedef size_t usize_t;
typedef void *addr_t;
#define forever for(;;)

void panic(const char *msg, ...);

#define likely(x) (__builtin_expect((x), 1))
#define unlikely(x) (__builtin_expect((x), 0))

#endif
