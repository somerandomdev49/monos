#ifndef __MONOS_USERSPLIB_BOOL_H
#define __MONOS_USERSPLIB_BOOL_H

typedef _Bool bool;
#define true 1
#define false 0

#endif
