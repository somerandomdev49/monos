# monos

> non-posix nanokernel, WIP

## building

### setting up limine

> requirements: git, make

1. download [limine binaries](https://github.com/limine-bootloader/limine#binary-releases)
2. go to the downloaded directory and compile `limine-deploy` with `make`
3. set `LIMINE_PATH` to the downloaded directory

### building the kernel

> requirements: limine, clang, nasm, ninja, python 3.9

1. go to the project directory
2. `<your favourite python3 executable> genbuld.py`
3. `ninja`

## running

> requirements: qemu

1. `qemu-system-x86_64 out.iso -serial stdio`
